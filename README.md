# Unix Primer - Basic Commands In the Unix Shell, 

Original document used with permission from Theoretical and Computational Biophysics Group, UIUC at https://www.ks.uiuc.edu/.

- Webpage: https://zwartell.gitlab.io/unix-primer/Unix%20Primer%20-%20Basic%20Commands%20In%20the%20Unix%20Shell.html

Git Branches 

- main - modified version for Dr. Wartell UNC Charlotte courses.
- Upstream - copy of original from UIUC used with permission from
    David J. Hardy, Ph.D.
    Beckman Institute
    University of Illinois at Urbana-Champaign
    405 N. Mathews Ave., Urbana, IL 61801

Copyright remains with University of Illinois at Urbana-Champaign
